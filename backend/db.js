const mysql=require('mysql2')

const openConnection=()=>{
    const connection=mysql.createConnection({
        host:"mydb",
        //uri: "mysql://db:3306",
        user:"root",
        password:"root",
        database:"demodb",
		waitForConnection: true
	})
	connection.connect()
    return connection
}

module.exports={
    openConnection,
}