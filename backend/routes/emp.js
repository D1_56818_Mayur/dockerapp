const { response } = require('express')
const express=require('express')
const db=require('../db')

const router=express.Router()

router.get('/get',(request,response)=>{
    const connection=db.openConnection()
    const statement=`select * from emp`
    connection.query(statement,(error,result)=>{
        if(error){
            response.send(error)
        }else{
            response.send(result)
        }
    })
})

router.post('/add',(request,response)=>{
	const{name, salary, age}=request.body
    const connection=db.openConnection()
    const statement=`insert into emp (name,salary,age) values('${name}',${salary},${age})`
    connection.query(statement,(error,result)=>{
        if(error){
            response.send(error)
        }else{
            response.send(result)
        }
    })
})

router.delete('/delete/:id',(request,response)=>{
    const {id}=request.params
    const statement=`delete from emp where id='${id}'`
    const connection=db.openConnection();
    connection.query(statement,(error,data)=>{
        if(error){
            response.send(error)
        }else{
            response.send(data)
        }
    })
})

module.exports=router