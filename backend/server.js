const express=require('express')
const cors=require('cors')
const routerEmp=require('./routes/emp')
const app=express()

app.use(express.json())
app.use(cors('*'))

app.use('/emp', routerEmp)

app.listen(4000,'0.0.0.0',()=>{
    console.log('server started on port no 4000')
})