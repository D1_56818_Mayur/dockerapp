CREATE TABLE emp(
    id integer primary key auto_increment,
    name VARCHAR(200),
    salary float,
    age integer
);

INSERT INTO emp (name,salary,age) VALUES('mayur',25000.00,29);
INSERT INTO emp (name,salary,age) VALUES('dinesh',35000.00,30);